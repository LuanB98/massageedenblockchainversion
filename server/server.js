var express = require("express");
var app = express();
var morgan = require("morgan");
var bodyParser = require("body-parser");
var braintree = require("braintree");
const cors = require("cors");

//app.use(cors());

//app.use(bodyParser.json());
//app.use(bodyParser.urlencoded({extended: false}));   # Original settings before Heroku intergration.

// Heroku setup
app.use(morgan("dev"));
app.use(bodyParser.urlencoded({ extended: "true" }));
app.use(bodyParser.json());
app.use(cors());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "DELETE, PUT");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

app.use(express.static("www"));
app.set("port", process.env.PORT || 5000);
app.listen(app.get("port"), function() {
  console.log("Express server listening on port " + app.get("port"));
});

//

var gateway = braintree.connect({
  environment: braintree.Environment.Sandbox,
  merchantId: "4gr3sw5hfbdydn2n",
  publicKey: "dtd7fwqg5wd7w3tj",
  privateKey: "1cf47c9f5ffd90d68179fb88551d23f0"
});

app.get("/", (req, res) => {
  res.send("message api is running");
});

app.get("/client_token", (req, res) => {
  gateway.clientToken.generate({}, (err, response) => {
    res.send(response.clientToken);
  });
});

app.post("/checkout", (req, res) => {
  console.log("obj", req.body);
  gateway.transaction.sale(
    {
      amount: req.body.amount,
      paymentMethodNonce: req.body.nonce,
      customFields: {
        provider_id: req.body.provider_id,
        provider_amount: req.body.amount,
        provider_email: "temp@email.com",
        provider_name: req.body.provider_name,
        transaction_date: new Date().toISOString()
      }
    },
    function(err, result) {
      res.send(result);
    }
  );
});

app.listen(process.env.PORT || "3000", () => {
  console.log("server is running");
});
