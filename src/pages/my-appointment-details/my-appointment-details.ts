import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { MessageDetailPage } from "../message-detail/message-detail";
import {UserPaymentPage} from "../user-payment/user-payment";
import { UserService } from "../../providers/user-service";
import {AngularFire, FirebaseObjectObservable} from 'angularfire2'

@Component({
  selector: 'page-my-appointment-details',
  templateUrl: 'my-appointment-details.html'
})
export class MyAppointmentDetailsPage {

  paid: FirebaseObjectObservable<any>;
  event = {
    timeStarts: '20:00'
  };

  clientRef:any;
  providerRef:any;
  appointmentRef:any;
  appointmentObj:any = {};
  userObj:any = {};
  constructor(public afRef: AngularFire, public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public userService:UserService) {
    let appointmentObjParam = this.navParams.get("appointment");
    this.clientRef = this.afRef.database.object("/users/"+appointmentObjParam.clientId+"/myAppointments/"+ appointmentObjParam.keyVal);
    var options = { weekday: 'short', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric' };

    /*if(userService.getUserDetails().role == "provider"){
      this.appointmentObj = appointmentObjParam;
      this.appointmentObj.dateTime = new Date(this.appointmentObj.dateTime).toLocaleString("en-US",options);
    }
    else {
      this.clientRef.subscribe(appointmentObj=>{
        this.appointmentObj = appointmentObj;
        this.appointmentObj.dateTime = new Date(this.appointmentObj.dateTime).toLocaleString("en-US",options);
      })
    }*/

    this.appointmentRef = this.afRef.database.object("/appointments/"+appointmentObjParam.$key);
    this.appointmentRef.subscribe(obj=>{
      this.appointmentObj = obj;
    })
    
    this.appointmentObj.dateTime = new Date(this.appointmentObj.dateTime).toLocaleString("en-US",options);
    this.userObj = this.userService.getUserDetails();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyAppointmentDetailsPage');
  }
  closeAppointment(){
    let confirm = this.alertCtrl.create({
      title: 'Close Booking',
      message: 'Are you sure? You want to close this booking?',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Ok',
          handler: () => {
            this.afRef.database.list("/appointments/").update(this.appointmentObj.$key,{serviceTaken: true});
          }
        }
      ]
    });
    confirm.present();
  }

  contactProvider(msgObj){
    this.navCtrl.push(MessageDetailPage,{providerInfo: {$key : msgObj.providerId, name: msgObj.providerName, clientName: this.appointmentObj.clientName, route:"appointmentDetails" }})
  }

  bookingForm(){
    this.navCtrl.push(UserPaymentPage,{amount: this.appointmentObj.serviceCharges.split("$")[1], appointmentObj: this.appointmentObj});
  }
}
