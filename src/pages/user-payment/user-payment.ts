import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
declare var braintree: any;
import {PaymentService} from '../../providers/payment-service';
import {AngularFire} from 'angularfire2';
import {UserService} from "../../providers/user-service";


@Component({
  selector: 'page-user-payment',
  templateUrl: 'user-payment.html'
})
export class UserPaymentPage {

  amount = "" ;
  userObj:any = {};
  appointmentRef:any = {};
  appointmentObj:any = {};
  showFormFields = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public userService: UserService, public paymentService: PaymentService, public afRef: AngularFire) {
    this.userObj = this.userService.getUserDetails();
    this.amount = this.navParams.get("amount");
    this.appointmentObj = this.navParams.get("appointmentObj");
    paymentService.getToken().subscribe((clientToken:any) =>{
      setTimeout(()=>this.showFormFields = true,2000)
      let that = this;
      braintree.setup(clientToken._body, "dropin", {
          container: "payment-form",
          onReady: ()=> {
          that.showFormFields = true;
          },
          onPaymentMethodReceived:  (obj)=> {
            let paymentObj = {
              nonce: obj.nonce,
              amount: this.amount,
              provider_id:this.appointmentObj. providerId,
              provider_amount: this.amount,
              provider_email: "temp@email.com",
              provider_name: this.appointmentObj.providerName,
              transaction_date: new Date().toISOString()
            }
            paymentService.sendToken(paymentObj).subscribe((result:any) =>{
              console.log(result);
              console.log(this.appointmentObj)
                this.appointmentRef = this.afRef.database.list("/appointments/");
                this.appointmentRef.update(this.appointmentObj.$key,{isBooked: true}) ;
                this.navCtrl.pop();
            })
          }
        });

})
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserPaymentPage');
  }

}
