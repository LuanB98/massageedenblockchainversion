import { Component, Inject } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";
import { UserService } from "../../providers/user-service";
import { AngularFire, FirebaseApp } from "angularfire2";
import { Camera } from "@ionic-native/camera";

@Component({
  selector: "page-provider-profile-settings",
  templateUrl: "provider-profile-settings.html"
})
export class ProviderProfileSettingsPage {
  userObj: any = {};
  private usersRef: any = {};
  showEditMode = true;
  public storageRef: any;
  fileName: any;
  constructor(
    @Inject(FirebaseApp) firebaseApp,
    private camera: Camera,
    public navCtrl: NavController,
    public navParams: NavParams,
    public userService: UserService,
    public afRef: AngularFire
  ) {
    this.userObj = userService.getUserDetails();
    this.userObj.rating = 5;
    this.storageRef = firebaseApp.storage().ref();
    if (!this.userObj.fullName) {
      this.userObj.fullName = "N/A";
    }
    if (!this.userObj.gender) {
      this.userObj.gender = "Male";
    }
    if (!this.userObj.rating) {
      this.userObj.rating = 5;
    }
    if (!this.userObj.location) {
      this.userObj.location = "North Perth";
    }
    if (!this.userObj.avgRates) {
      this.userObj.avgRates = "N/A";
    }
    if (!this.userObj.experience) {
      this.userObj.experience = "N/A";
    }
    if (!this.userObj.serviceRadius) {
      this.userObj.serviceRadius = 20;
    }
    this.userObj.addOtherLocations = false;

    this.usersRef = this.afRef.database.list("/users");
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad ProviderProfileSettingsPage");
  }

  selectPicture() {
    this.camera
      .getPicture({
        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        destinationType: this.camera.DestinationType.DATA_URL,
        quality: 100,
        encodingType: this.camera.EncodingType.JPEG
      })
      .then(
        imageData => {
          this.storageRef
            .child(this.userObj.$key)
            .putString(imageData, "base64", { contentType: "image/jpeg" })
            .then(data => {
              // alert(data.downloadURL);
              this.userObj.imageUrl = data.downloadURL;
              this.saveProfileSettings();
            });
        },
        error => {
          console.log("ERROR -> " + JSON.stringify(error));
        }
      );
  }

  saveProfileSettings() {
    if (!this.userObj.desc || this.userObj.desc.trim() === "") {
      this.userObj.desc = "N/A";
    }
    this.showEditMode = true;
    let key: String = this.userObj.$key;
    this.usersRef
      .update(key, {
        profileStatus: "published",
        avgRates: this.userObj.avgRates,
        desc: this.userObj.desc,
        experience: this.userObj.experience,
        gender: this.userObj.gender,
        location: this.userObj.location,
        fullName: this.userObj.fullName,
        imageUrl: this.userObj.imageUrl || null,
        walletAddress: this.userObj.walletAddress,
        serviceRadius: parseInt(this.userObj.serviceRadius)
      })
      .then(
        () => {
          alert("Saved changes successfully");
          this.userObj.profileStatus = "published";
          this.userService.setUserDetails(this.userObj);
        },
        err => {
          alert(err.message);
        }
      );
  }
}
