import { Component } from "@angular/core";
import { NavController, AlertController, Platform } from "ionic-angular";
import { MenuPage } from "../menu/menu";
import { SignupPage } from "../signup/signup";
import { AngularFire } from "angularfire2";
import { UserService } from "../../providers/user-service";
import { LoadingController } from "ionic-angular";

@Component({
  selector: "page-home",
  templateUrl: "login.html"
})
export class LoginPage {
  loginObj = {
    email: "",
    password: ""
  };
  loginDeviceKey: string = "";

  loading: any = {};
  constructor(
    public navCtrl: NavController,
    public afRef: AngularFire,
    public userService: UserService,
    public loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private platform: Platform
  ) {}

  showPlatform() {
    let text = "I run on: " + this.platform.platforms();
    let alert = this.alertCtrl.create({
      title: "My Home",
      subTitle: text,
      buttons: ["Ok"]
    });
    alert.present();
  }

  ionViewDidLoad() {}

  login() {
    this.loading = this.loadingCtrl.create({
      content: "Signing in...",
      spinner: "crescent"
    });

    let that = this;
    if (
      this.loginObj.email.trim() != "" &&
      this.loginObj.password.trim() != ""
    ) {
      this.loading.present();
      this.afRef.auth
        .login({ email: this.loginObj.email, password: this.loginObj.password })
        .then(
          auth => {
            this.afRef.database
              .list("/users")
              .update(auth.uid, { deviceToken: this.loginDeviceKey });
            this.afRef.database.object("/users/" + auth.uid).subscribe(data => {
              this.loading.dismiss();
              let userData: any = data;
              let userKey: any = data.$key;
              userData.myKey = userKey;
              localStorage.setItem("userObj", JSON.stringify(userData));
              let isAuthenticated = this.userService.isAlreadyAuthenticated();
              if (isAuthenticated) {
                this.userService.setUserDetails(data);
                return;
              }
              this.userService.setUserDetails(data);
              alert("You have latest built");
              if (data.role == "provider") {
                this.navCtrl.push(MenuPage, {
                  username: this.loginObj.email,
                  role: "provider"
                });
              } else {
                this.navCtrl.push(MenuPage, {
                  username: this.loginObj.email,
                  role: "client"
                });
              }
              this.loginObj = {
                email: "",
                password: ""
              };
              this.userService.setUserDetails(data);
            });
          },
          function(err) {
            that.loading.dismiss();
            console.log(err);
            alert(err.message);
          }
        );
    }
  }

  gotoSignup() {
    this.navCtrl.push(SignupPage);
  }
}
