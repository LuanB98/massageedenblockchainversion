import { Component } from '@angular/core';
import { NavController, NavParams, MenuController } from 'ionic-angular';
import { DashbordPage } from "../dashbord/dashbord";
import { MyAppointmentsPage } from "../my-appointments/my-appointments";
import { ClientProfilePage } from "../client-profile-settings/client-profile";
import { MessagesListPage } from "../messages-list/messages-list";
import { UserService } from "../../providers/user-service";
import {ProviderProfileSettingsPage} from "../provider-profile-settings/provider-profile-settings";
import {LoginPage} from "../login/login";

@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html'
})
export class MenuPage {

  rootPage:any = {};
  isTherapist: boolean = true;
  disableClientMenu:boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams, public menuCtrl: MenuController, public userService: UserService) {
    let clientRole = this.navParams.get("role");

    if(!clientRole && localStorage.getItem("userObj")){
      let userAuthData = JSON.parse(localStorage.getItem("userObj"));
      clientRole = userAuthData.role;
    }

    this.userService.setUserRole(clientRole);

    if(clientRole && clientRole == 'provider'){
      //Role is "Therapist"
      this.rootPage = MyAppointmentsPage;
      this.disableClientMenu = false;
    }
    else {
      this.rootPage = DashbordPage;
      this.disableClientMenu = true;
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MenuPage');
  }

  openPage(screenInfo){
    if(screenInfo == 'LoginPage') {
      this.menuCtrl.close();
      this.navCtrl.setRoot(LoginPage);
      // this.navCtrl.pop();
      // setTimeout(()=>{
      //   this.navCtrl.push(LoginPage);
      //   // this.navCtrl.popToRoot();
      // });
      this.userService.logout();
    }

    else if(screenInfo == 'MyAppointmentsPage') {
      if(!this.disableClientMenu){
        this.menuCtrl.close();
        return;
      }

      this.menuCtrl.close();
      setTimeout(()=>{
        this.navCtrl.push(MyAppointmentsPage);
      },300);
    }

    else if(screenInfo == 'ClientProfilePage') {
      this.menuCtrl.close();
      if(!this.disableClientMenu){
        setTimeout(()=>{
          this.navCtrl.push(ProviderProfileSettingsPage);
        },300);
      }
      else {
        setTimeout(()=>{
          this.navCtrl.push(ClientProfilePage);
        },300);
      }
    }

    else if(screenInfo == 'MessagesListPage'){
      this.menuCtrl.close();
      setTimeout(()=>{
        this.navCtrl.push(MessagesListPage);
      },300);
      }

    else
      this.menuCtrl.close();
  }


}
