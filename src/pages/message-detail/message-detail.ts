import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ActionSheetController } from 'ionic-angular';
import { UserService } from "../../providers/user-service";
import { ServiceQuotePage } from "../service-quote/service-quote";
import { ClientHistoryPage } from "../client-history/client-history";
// import { UserPaymentPage } from "../user-payment/user-payment";
import { AngularFire } from "angularfire2";
import { Content } from 'ionic-angular/index';

@Component({
  selector: 'page-message-detail',
  templateUrl: 'message-detail.html',
})
export class MessageDetailPage {

  @ViewChild(Content) content: Content;

  messageChannelNode: any = {};
  isProviderProfile: boolean = false;
  senderObj: any = {};
  createChannelNode: boolean;
  userObj: any = {};
  msgText: string = "";
  channelNode: string = "";
  channelRef: any;
  userChatList: any = [];
  messageTitle = "";
  route = "";
  msgObj: any;
  userMessageNodeRef: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public userService: UserService, public actionSheetCtrl: ActionSheetController, public afRef: AngularFire, ) {

    this.senderObj = this.navParams.get("providerInfo");
    this.channelNode = this.navParams.get("channelNode");
    this.messageTitle = this.navParams.get("messageTitle");
    this.msgObj = this.navParams.get("msgObj")

    if (this.senderObj.route) {
      this.route = this.senderObj.route;
      this.msgObj = this.navParams.get("providerInfo");
    }

    this.userObj = this.userService.getUserDetails();

    if (this.channelNode && this.channelNode != '') {
      this.channelRef = this.afRef.database.list("/messageChannels/" + this.channelNode + "/messages");
      this.channelRef.subscribe(data => {
        this.userChatList = Object.keys(data).map(function (key) { return data[key]; });
        console.log(this.userChatList);
      })
    }
    else {
      this.userMessageNodeRef = this.afRef.database.list("/users/" + this.userObj.$key + "/messages");
      this.userMessageNodeRef.subscribe(data => {
        let allNodesChat = data;
        let thisChatDetails = allNodesChat.filter(msgNode => {
          if (msgNode.senderId == this.senderObj.$key && msgNode.receiverId == this.userObj.$key ||
            msgNode.senderId == this.userObj.$key && msgNode.receiverId == this.senderObj.$key)
            return msgNode;
        });
        if (thisChatDetails.length) {
          this.channelNode = thisChatDetails[0].channelNode;   //this will be and should be only 1.
          this.channelRef = this.afRef.database.list("/messageChannels/" + this.channelNode + "/messages");
          this.channelRef.subscribe(messages => {
            this.userChatList = Object.keys(messages).map(function (key) { return messages[key]; });
          })
        }
        else this.createChannelNode = true;
      })
    }
    if (this.userObj.role == 'provider') {
      this.isProviderProfile = true;
    }
    this.messageChannelNode = this.afRef.database.list("/messageChannels");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MessageDetailPage');
  }

  ionViewDidEnter() {
    this.content.scrollToBottom(0);
  }

  showActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      // title: 'Modify your album',
      buttons: [
        {
          text: 'Send Quotation',
          handler: () => {
            this.navCtrl.push(ServiceQuotePage, { clientId: this.senderObj.$key, "messageTitle": this.messageTitle, "clientName": this.msgObj.senderName, "route": this.route });
            console.log('Send Quotation clicked');
          }
        }, {
          text: 'View Client History',
          handler: () => {
            this.navCtrl.push(ClientHistoryPage, { clientId: this.senderObj.$key, "messageTitle": this.messageTitle, "clientName": this.msgObj.senderName, "msgObj": this.msgObj, "route": this.senderObj.route });
            console.log('client profile clicked');
          }
        }, {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  sendMessage() {
    let senderRef = this.afRef.database.list("/users/" + this.senderObj.$key + "/messages/");
    let receiverRef = this.afRef.database.list("/users/" + this.userObj.$key + "/messages/");
    let refNode = this.afRef.database.list("/messageChannels")
    if (this.msgText.trim() == "")
      return;
    if (this.createChannelNode) {
      let newMsg = this.messageChannelNode.push({
        senderId: this.userObj.$key,
        receiverId: this.senderObj.$key,
        senderName: this.userObj.fullName,
        receiverName: this.senderObj.fullName,
        seenByReceiver: false,
        timestamp: new Date().toISOString()
      })
      this.channelNode = newMsg.getKey()
      let msgNode = {
          channelNode: this.channelNode,
          senderId: this.userObj.$key,
          receiverId: this.senderObj.$key
        }
        senderRef.push(msgNode);
        receiverRef.push(msgNode);
      this.createChannelNode = false;
      this.channelRef = this.afRef.database.list("/messageChannels/" + this.channelNode + "/messages");
    }
    this.channelRef.push({ text: this.msgText, sender: this.userObj.$key, timestamp: new Date().toISOString() });

    //Update new message pointer
    refNode.update(this.channelNode,{ seenByReceiver: false, timestamp: new Date().toISOString(),receiverId: this.senderObj.$key })

    this.msgText = "";
    this.content.scrollTo(0, this.content.scrollHeight, 500)
    this.channelRef.subscribe(data=>{
      console.log(data)
      this.userChatList = data;
    })
  }

  findMe(id) {
    return id == this.userObj.$key ? true : false;
  }
}
