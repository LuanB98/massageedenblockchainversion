import { AuthProviders, AuthMethods } from 'angularfire2';

export const firebaseConfig = {
  apiKey: "AIzaSyA0RlhqZ2lzW0C3DCRGuVhKxgVFEWm6XKk",
  authDomain: "massageapp-af1b2.firebaseapp.com",
  databaseURL: "https://massageapp-af1b2.firebaseio.com",
  projectId: "massageapp-af1b2",
  storageBucket: "massageapp-af1b2.appspot.com",
  messagingSenderId: "1092635476413"
};

export const myFirebaseAuthConfig = {
  provider: AuthProviders.Password,
  method: AuthMethods.Password
};
