import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Subject } from 'rxjs';

@Injectable()
export class UserService {

  constructor(public http: Http) {
  }

  private userRole: any = {};
  private isAuthenticated = false;
  private userObj: any = {};
  private deviceToken: string = "";
  public deviceTokenObserver$: Subject<any> = new Subject<any>();
  public userObserver$: Subject<any> = new Subject<any>();

  getUserDetails() {
    this.isAuthenticated = true;
    if (this.userObj.$key)
      return this.userObj;
    else if (localStorage.getItem("userObj")) {
      this.userObj = JSON.parse(localStorage.getItem("userObj"));
      this.userObj.$key = this.userObj.myKey;
      return this.userObj;
    }
  }

  setUserDetails(userObj) {
    this.userObj = userObj;
    this.isAuthenticated = true;
    localStorage.setItem("userObj", JSON.stringify(userObj));
    this.userObserver$.next(userObj);
  }

  getUserRole() {
    return this.userRole;
  }

  setUserRole(userRole) {
    this.userRole = userRole;
  }

  isAlreadyAuthenticated() {
    return this.isAuthenticated ? true : false;
  }

  registerDeviceToken(token) {
    if (!token || token && token.trim() == "")
      return false;

    this.deviceToken = token || "N/A";
    console.log("-------------Token-----------------");
    console.log(token);
    console.log("-------------Token-----------------");
    this.deviceTokenObserver$.next(this.deviceToken);
  }

  logout() {
    this.isAuthenticated = false;
    this.userObj = {};
    localStorage.removeItem("userObj");
  }
}
