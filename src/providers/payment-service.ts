import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';

@Injectable()
export class PaymentService {

  constructor(public http: Http) {}

  getToken(){
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get('https://edenmessage.herokuapp.com/client_token', {headers: headers})
  }

  sendToken(paymentObj){
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('https://edenmessage.herokuapp.com/checkout',paymentObj, {headers: headers})
  }

}