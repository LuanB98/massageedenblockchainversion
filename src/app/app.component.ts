import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';
import { FCM } from '@ionic-native/fcm';

import { LoginPage } from '../pages/login/login';
import { MenuPage } from "../pages/menu/menu";

import { UserService } from '../providers/user-service';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any;
  enableNotification:boolean;

  constructor(platform: Platform, private fcm: FCM, public userService: UserService) {
    if (localStorage.getItem("userObj")) {
      this.rootPage = MenuPage;
    }
    else this.rootPage = LoginPage;

    if(platform.is('mobileweb') || platform.is('core')){
        this.enableNotification = false;
      }
      else this.enableNotification = true;

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();

      if (this.enableNotification) {
        fcm.subscribeToTopic('marketing');

        fcm.getToken().then(token => {
          userService.registerDeviceToken(token);
        })

        fcm.onNotification().subscribe(data => {
          if (data.wasTapped) {
            console.log("Received in background");
          } else {
            console.log("Received in foreground");
          };
        })

        fcm.onTokenRefresh().subscribe(token => {
          userService.registerDeviceToken(token);
        })

        fcm.unsubscribeFromTopic('marketing');
      }

    });
  }
}
